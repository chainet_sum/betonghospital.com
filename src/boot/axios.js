import Vue from "vue";
import axios from "axios";

const axiosInstance = axios.create({
  // ถือโอกาสเซ็ต baseurlไปด้วยเลยได้่เขียนสั้นๆใน ตอนยิง axiosในหน้า vue
  baseURL: "http://localhost:80/betonghospitaldb/public/",
  // เซ็ตตัวนี้เพื่อให้ใช้session ได้ในการ รับส่ง แบบ cross ข้ามพื้นที่อะ ไปยังapiแล้วกลับมา ไม่งั้น sessionมันจะ รีใหม่ตลอด
  withCredentials: true
});

Vue.prototype.$axios = axios;

export { axiosInstance };

//https://www.betonghospital.net ตอนลงเซิฟใช้ตัวนี้ก่อน build ที่ baseURL: เพราะดึงข้อมูลจากapiคนละตัวกับในserver
