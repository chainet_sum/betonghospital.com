const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "history", component: () => import("pages/history.vue") },
      { path: "service", component: () => import("pages/service.vue") },
      { path: "contact", component: () => import("pages/contact.vue") },
      { path: "login", component: () => import("pages/login.vue") },
      { path: "manage", component: () => import("pages/manage.vue") }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
